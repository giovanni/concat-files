/**
 * Created by giovanni on 20/12/13.
 */
import java.io.{PrintWriter, FileWriter, File}
import scala.io.Source

object Main {

  def main(args: Array[String]) {
    val dir = "/home/giovanni/Documentos/teste"
    println(dir)
    val fileList = new File(dir).listFiles.map(_.getName).toList
    // auxiliar para verificar se o arquivo já foi visitado
    var visitados: Set[String] = Set()
    // auxiliar para verificar se o arquivo já foi salvo
    var salvos: Set[String] = Set()

    fileList.foreach(concatenar)

    def concatenar(file:String) = {
      var arquivosIguais = new File(dir).listFiles().filter(_.getName().startsWith(file.substring(0,4))).toList
      // Ordena os arquivos por nome
      arquivosIguais = arquivosIguais.sortWith(_.getName < _.getName)
      var line : String = ""
      var nomeArquivo :String = ""
      arquivosIguais.zipWithIndex foreach {case (a,i) => {

        // Apenas concatena uma vez
        if(!visitados.contains(a.getName)){

          if(i != 0){
            for(line2 <- Source.fromFile(s"/$a").getLines().drop(1)){
              line += line2 + "\n"
            }
          } else {
            for(line2 <- Source.fromFile(s"/$a").getLines()){
              line += line2 + "\n"
            }
          }

        }
        visitados = visitados ++  Set(a.getName)  // mantem visitados atualizado
        nomeArquivo = a.getName.substring(0,4)

      }}

      salvar(nomeArquivo, line)
    }

    def salvar(name:String, line : String) = {
      if(!salvos.contains(name)){
        val writer = new PrintWriter(new File(s"$dir/../$name"))
        println(s"$name ----------------")
        println(line)
        writer.write(line)
        //      writer.flush()
        writer.close()
        salvos = salvos ++ Set(name)
      }

    }

  }

}
